import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class SomeClass {
    public static Logger LOGGER = LoggerFactory.getLogger(SomeClass.class);
    public static void LogExample() {
        LOGGER.info("Вы видите этот текст в конце теста среди логов");
    }
}
